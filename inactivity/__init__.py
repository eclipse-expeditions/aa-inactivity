""""Activity monitoring app for Alliance Auth."""

# pylint: disable = invalid-name
default_app_config = "inactivity.apps.InactivityConfig"

__version__ = "1.1.1"

__title__ = "Inactivity"
